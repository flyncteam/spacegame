﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameControllerScript : MonoBehaviour {

	public static int level = 1;
	public GameObject bullet;
	public Transform bulletSpawn;
	public float fireRate;
	Animator animPlayer;
	private float nextFire;
	public GameObject player;
	public Text textLevel;
	public bool shot = false;

	// Use this for initialization
	void Start () {
		animPlayer = player.GetComponent<Animator> ();
		textLevel.text = "Level: " + level;
		shot = false;
	}

	// Update is called once per frame
	void FixedUpdate () {
		if (animPlayer == null) {
			return;
		}

		if (shot && Time.time > nextFire && animPlayer.GetBool("Ground")) {
						nextFire = Time.time + fireRate;
						Instantiate (bullet, bulletSpawn.position, bulletSpawn.rotation);
						animPlayer.SetBool ("Shot", true);
		} else {
				animPlayer.SetBool ("Shot", false);
		}

	}

	public void ShootUp()
	{
		shot = false;
	}

	public void ShootDown()
	{
		shot = true;
	}


}
