﻿using UnityEngine;
using System.Collections;

public class DestroyPlayer : MonoBehaviour {

	public GameObject explosion;

	void OnCollisionEnter2D(Collision2D other) 
	{
		if (other.collider.tag == "Stone" || other.collider.tag == "Monster"
		    || other.collider.tag == "Geyser") {
			Instantiate(explosion, transform.position, transform.rotation);
			Destroy(gameObject);	
		}
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.tag == "EndGame") {
			Application.LoadLevel(++GameControllerScript.level);		
		}
	}


}
