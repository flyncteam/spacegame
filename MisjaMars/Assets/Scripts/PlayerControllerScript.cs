﻿using UnityEngine;
using System.Collections;

public class PlayerControllerScript : MonoBehaviour {

	public float maxSpeed = 5f;
	Animator animPlayer;

	public bool grounded = false;
	public Transform groundCheck;
	float groundRadius = 0.2f;
	public LayerMask whatIsGround;
	public float jumpForce = 560f;
	private bool stay = false;

	// Use this for initialization
	void Start () {
		animPlayer = GetComponent<Animator> ();
		stay = false;
	}
	
	// Update is called once per frame
	void FixedUpdate () {

		if (animPlayer == null) {
			return;
		}

		grounded = Physics2D.OverlapCircle (groundCheck.position, groundRadius, whatIsGround);
		animPlayer.SetBool ("Ground", grounded);

		animPlayer.SetFloat ("vSpeed", rigidbody2D.velocity.y);
	

	
		if (!stay) {
						rigidbody2D.velocity = new Vector2 (maxSpeed, rigidbody2D.velocity.y);
			animPlayer.SetFloat ("Speed", Mathf.Abs (maxSpeed));
		} else {
			animPlayer.SetFloat("Speed", Mathf.Abs(0f));
		}
	}

	void Update()
	{
		/*
		if (grounded && Input.GetKeyDown (KeyCode.Space)) 
		{
			animPlayer.SetBool("Ground", false);
			rigidbody2D.AddForce(new Vector2(0, jumpForce));
		}*/
	}

	public void Jump()
	{

		if (animPlayer == null) {
			return;
		}

		if (grounded) 
		{
			animPlayer.SetBool("Ground", false);
			rigidbody2D.AddForce(new Vector2(0, jumpForce));
		}
	}

	public void StayUp()
	{
		stay = false;
	}


	public void StayDown()
	{
		stay = true;
	}
}
