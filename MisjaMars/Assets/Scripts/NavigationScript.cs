﻿using UnityEngine;
using System.Collections;

public class NavigationScript : MonoBehaviour {

	public void NavigateToGame()
	{
		Application.LoadLevel(GameControllerScript.level);
	}

	public void EndGame()
	{
		Application.Quit ();
	}

	public void NavigateToMenu()
	{
		GameControllerScript.level = 1;
		Application.LoadLevel(0);
	}
}
