﻿using UnityEngine;
using System.Collections;

public class MusicBackgroundScript : MonoBehaviour {

	private static MusicBackgroundScript instance = null;
	public static MusicBackgroundScript Instance {
		get { return instance; }
	}
	void Awake() {
		if (instance != null && instance != this) {
			Destroy(this.gameObject);
			return;
		} else {
			instance = this;
		}
		DontDestroyOnLoad(this.gameObject);
	}
}
