﻿using UnityEngine;
using System.Collections;

public class DestroyMonsterByBullet : MonoBehaviour {

	public GameObject explosion;
	
	void OnCollisionEnter2D(Collision2D other) 
	{
		if (other.collider.tag == "Bullet") {
			Instantiate(explosion, transform.position, transform.rotation);
			Destroy(other.gameObject);
			Destroy(gameObject);		
		}
	}
}
