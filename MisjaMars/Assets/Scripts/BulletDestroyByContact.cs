﻿using UnityEngine;
using System.Collections;

public class BulletDestroyByContact : MonoBehaviour {

	public GameObject explosion;
	
	void OnCollisionEnter2D(Collision2D other) 
	{
		if (other.collider.tag == "Stone" || other.collider.tag == "Geyser") {
			Quaternion quat = transform.rotation;
			quat.z = -180;
			Instantiate(explosion, transform.position, quat);
			Destroy(gameObject);		
		}
	}
}